#!/usr/bin/env python
#
# FireSql a detection and protection sql injection engine.
#                                                              
# Copyright (C) 2012  Luis Campo Giralte 
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the
# Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
# Boston, MA  02110-1301, USA.
#
# Written by Luis Campo Giralte <luis.camp0.2009@gmail.com> 2012 
#
"""Example script for integrate the firesql with other systems """
__author__ = "Luis Campo Giralte"
__copyright__ = "Copyright (C) 2012 by Luis Campo Giralte"
__revision__ = "$Id$"
__version__ = "0.1"

import signal 
import sys
import pyfiresql
from optparse import OptionParser

proxy = None

def parseOptions():
	"""Parse the user options"""

	usage = "Usage: %prog [options]"
	
	p = OptionParser(usage)

	p.add_option("-l", "--localip", dest="localip", default=None,
		help="Sets the local address of the proxy.")
	p.add_option("-p", "--localport", dest="localport", default=None,
		help="Sets the local port of the proxy.")
	p.add_option("-r", "--remoteip", dest="remoteip", default=None,
		help="Sets the remote address of the database.")
	p.add_option("-q", "--remoteport", dest="remoteport", default=None,
		help="Sets the remote port of the database.")

	return p

def signalHandler(num):

	print "signalHandler", num
	if(proxy):
		proxy.stop()

	sys.exit(0)		


if __name__ == '__main__':


	parser = parseOptions()
    	(opt, args) = parser.parse_args()

        if(opt.localip == None):
                parser.error("Argument localip required")
                sys.exit(-1)

        if(opt.localport == None):
                parser.error("Argument localport required")
                sys.exit(-1)

        if(opt.remoteip == None):
                parser.error("Argument remoteip required")
                sys.exit(-1)

        if(opt.remoteport == None):
                parser.error("Argument remoteport required")
                sys.exit(-1)

	signal.signal(signal.SIGINT,signalHandler)
	signal.signal(signal.SIGQUIT,signalHandler)

	proxy = pyfiresql.Proxy(opt.localip,int(opt.localport),opt.remoteip,int(opt.remoteport))

	rul = pyfiresql.RuleManager.getInstance()

	# Read the rules from a database?
	#rul.addRule("query","print")
	# Some usefull regex, the order mathers

	# Query to detect 
	rul.addRule("^SELECT.*WHERE username='' OR ''=''","reject")

	# Query to detct
	# SELECT userid, username FROM sql_injection_test WHERE userid=0 or 1	
	rul.addRule("^select.*where userid=[0-9]+ or","reject")

	# just for printing something
	rul.addRule("^select","print")

		
	proxy.start()

	#try:
	proxy.run()
	#except KeyboardInterrupt:
	proxy.stop()	

	sys.exit(0)	
