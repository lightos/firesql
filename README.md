FireSql a mysql firewall 
=========

The main goal of FireSql is to provide firewall services for
mysql/mariadb databases by using regular expressions and other rules.

FireSql system is based on c++11 and boost and have been tested on 
Linux platforms and Freebsd. 

Using FireSql 
---------------

To use FireSql just execute the binary firesql:


	luis@luis-xps:~/tmp$ ./firesql 
	FireSql 0.0.1
	Error: missing required option config
	Mandatory arguments:
	  -c [ --config ] arg   set the configure file  of the proxy.

	Optional arguments:
	  -D [ --daemon ]       Daemonize the process.
	  --help                show help
	  -v [ --version ]      show version string


Compile FireSql
----------------

    $ git clone https://bitbucket.com/camp0/firesql
    $ ./autogen.sh
    $ ./configure
    $ make

Configure FireSql
-----------------

The configuration file have the following sections:
	
	[Server]
	proxy_address=<local address of the proxy>
	proxy_port=<local port of the proxy>

	database_address =<remote address of the mysql database> 
	database_port =<remote port of the mysql database> 

	[Rules]
	# The rule files only contains regex of queries
	# The evaluation of the rules is linear, so order matters.
	print=./print.rule
	drop=./drop.rules
	reject=./reject.rules
	close=./close.rules


Contributing to FireSql 
-------------------------

FireSql is under the terms of GPLv2 and is under develop.

Check out the FireSql source with 

    $ git clone git://github.com/camp0/firesql
